<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Imagick;
use Storage;
use App\Book;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('profile.home.index');
    }

    public function viewBook($id){
        return view('profile.home.book-view',['id'=>$id]);
    }

    public function downloadBook($id){
        $book = Book::find($id);
        return Storage::download($book->url);
    }
}

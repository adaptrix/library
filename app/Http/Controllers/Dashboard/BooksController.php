<?php

namespace App\Http\Controllers\Dashboard;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{
    //
    public function index(){
        return view('dashboard.books.index');
    }

    public function addForm(){
        return view('dashboard.books.add');
    }

    public function editForm($id){
        return view('dashboard.books.edit');
    }

    public function edit(Request $request){
        dd($request);
    }

    public function add(Request $request){
        $book = new Book;
        $book->title = $request->name;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->category_id = $request->category_id;
        $book->description = $request->description;
        $book->url = $request->book->store('Books');
        $book->thumbnail = $request->thumbnail->store('Thumbnails');
        $book->save();
        return redirect('dashboard/books/add')->with('success','Book added successfully');
    }

    public function delete($id){
        dd($id);
    }
}

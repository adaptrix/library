<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    //
    public function index(){
        return view('dashboard.categories.index');
    } 
    public function add(Request $request){
        $cat  = new Category;
        $cat->name = $request->name;
        $cat->save();
        return redirect('/dashboard/categories')->with('success','Category created successfully');
    }
    public function editForm($id){
        return view('dashboard.categories.edit',['id'=>$id]);
    }
    public function edit(Request $request){
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->save();
        return redirect('/dashboard/categories')->with('success','Category eited successfully');
    }
    public function delete($id){
        dd($id);
    }
}

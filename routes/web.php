<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 

Auth::routes();
Route::middleware('auth')->group(function()
{
    Route::get('/',function()
    {
        if(Auth::user()->hasRole('superadmin'))
        {        
            return redirect('/dashboard');
        }
        if(Auth::user()->hasRole('user'))
        {        
            return redirect('/profile');
        } 
    });

    Route::prefix('profile')->namespace('Profile')->middleware(['App\Http\Middleware\UserMiddleware'])->group(function(){
        Route::get('/','HomeController@index')->name('profile'); 
        Route::get('/book/{id}','HomeController@viewBook');
        Route::get('/book/download/{id}','HomeController@downloadBook');
    });

    Route::prefix('dashboard')->namespace('Dashboard')->middleware(['App\Http\Middleware\SuperAdminMiddleware'])->group(function(){
        Route::get('/','DashboardController@index')->name('dashboard'); 
        Route::prefix('categories')->group(function(){
            Route::get('/','CategoriesController@index');
            Route::get('/edit/{id}','CategoriesController@editForm');
            Route::post('/edit','CategoriesController@edit'); 
            Route::post('/add','CategoriesController@add');
            Route::post('delete/{id}','CategoriesController@delete');
        });
        Route::prefix('books')->group(function(){
            Route::get('/','BooksController@index');
            Route::get('edit/{id}','BooksController@editForm');
            Route::post('edit','BooksController@edit');
            Route::get('add','BooksController@addForm');
            Route::post('add','BooksController@add');
            Route::get('delete/{id}','BooksController@delete');
        });
        Route::prefix('users')->group(function(){
            Route::get('/','UsersController@index');
        });
        Route::prefix('stats')->group(function(){
            Route::get('/','StatsController@index');
        });
        Route::prefix('reports')->group(function(){
            Route::get('/','ReportsController@index');
        });
    });
});
Route::get('/logout','Controller@logout')->name('logout');






// Route::get('/notallowed',function(){
//     return view('layouts.no-access');
// })->name('notallowed');

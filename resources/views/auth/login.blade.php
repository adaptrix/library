@extends('layouts.app')

@section('body')
<div class="ui middle aligned center aligned grid">
    <div class="column" style="max-width:450px;padding-top:200px;">
        <form class="ui large form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="ui stacked segments">
                <div class="ui segment">
                    Login
                </div>
                <div class="ui segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail Adress" required autofocus>
                     
                    </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                </div>

                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div> --}}

                <div class="field">
                        <button type="submit" class="ui fluid large teal submit button">
                            {{ __('Login') }}
                        </button>
                </div>
                </div>
            </div>
        </form>
        <div class="ui message">
            <a href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
            Or
            <a href="{{ route('password.request') }}">
                Register
            </a>
        </div>
    </div>
</div>
@endsection

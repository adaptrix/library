@php 
    use App\Book;
    $books = Book::all();
@endphp
@extends('dashboard.home')
@section('content')
<div class="card">
    <div class="header">
        <h2>
            BOOK LIST
          </h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{url('dashboard/books/add')}}" class="btn bg-teal">Add Book</a>
        </ul>
    </div>
    <div class="body table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Author</th>
                    <th>Publisher</th>
                </tr>
            </thead>
            <tbody>
                    @foreach($books as $book)
                <tr>
                    <td>{{$book->id}}</td>
                    <td>{{$book->title}}</td>
                    <td>{{$book->category->name}}</td>
                    <td>{{$book->author}}</td>
                    <td>{{$book->publisher}}</td>
                </tr>  
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
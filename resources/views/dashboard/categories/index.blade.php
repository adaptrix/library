@php 
    use App\Category;
    $categories = Category::all();
@endphp
@extends('dashboard.home')
@section('content')
@if(session('error'))
<div class="alert bg-red alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{session('error')}}
</div>
@endif
@if(session('success'))
<div class="alert bg-teal alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    {{session('success')}}
</div>
@endif
<div class="card">
        <div class="header">
            <h2>
                ADD CATEGORY
            </h2> 
        </div>
        <div class="body">
            <form method="POST" action="{{url('dashboard/categories/add')}}">
                {{csrf_field()}}
                <label for="name">Name</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Category Name">
                    </div>
                </div>   
                <button type="submit" class="btn bg-teal m-t-15 waves-effect">SAVE</button>
            </form>
        </div>
    </div>
<div class="card">
    <div class="header">
        <h2>
            CATEGORIES
          </h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{url('dashboard/categories/add')}}" class="btn bg-teal">Add Category</a>
        </ul>
    </div>
    <div class="body table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th> 
                </tr>
            </thead>
            <tbody>                
                @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->name}}</td> 
                    <td><a href="{{url('dashboard/categories/edit',$category->id)}}" class="btn bg-indigo">
                    Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@php
    $category = App\Category::find($id);
@endphp
@extends('dashboard.home')
@section('content')

<div class="card">
        <div class="header">
            <h2>
                EDIT CATEGORY
            </h2> 
        </div>
        <div class="body">
            <form method="POST" action="{{url('dashboard/categories/edit')}}">
                {{csrf_field()}}
                <label for="name">Name</label>
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" id="name" value="{{$category->name}}" name="name" class="form-control" placeholder="Category Name">
                    </div>
                </div>   
                <input type="hidden" name="id" value="{{$id}}">
                <button type="submit" class="btn bg-teal m-t-15 waves-effect">SAVE</button>
            </form>
        </div>
    </div>
@endsection
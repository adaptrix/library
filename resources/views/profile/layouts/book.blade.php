@php 
use Illuminate\Support\Facades\Storage;
@endphp
<div class="column">
    <div class="ui fluid card">
        <div class="image">
            <img src="{{$book->thumbnail?asset('storage/'.$book->thumbnail):asset('img/book-placeholder.png')}}" style="max-height:200px;">
        </div>
        <div class="content">
            <a href="{{url('profile/book',$book->id)}}" class="header" style="font-size:12px;">
                {{ str_limit($book->title, $limit = 25, $end = '...') }}
            </a>
            {{-- <div class="meta">
                <span class="date">Joined in 2013</span>
            </div> --}}
            <div class="description" style="font-size:10px;">
                {{ str_limit($book->author, $limit = 24, $end = '...') }}
            </div>
        </div>
        {{-- <div class="extra content">
            <a>
                <i class="user icon"></i>
                22 Friends
            </a>
        </div> --}}
    </div>
</div>

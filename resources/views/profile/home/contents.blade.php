@extends('layouts.app')

@section('body')
<div style="background-image:url('{{asset('img/bg.jpg')}}');background-size:cover;background-repeat:no-repeat;">
    @include('profile.layouts.navbar')
    @yield('contents')
</div>
<div class="footer">
    
</div>
@endsection

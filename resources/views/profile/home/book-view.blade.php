@php
    use App\Category;
    use App\Book;
    $bk = Book::find($id);
    $books = Book::take(4)->orderBy('created_at','desc')->get();
    $categories = Category::all();
@endphp
@extends('profile.home.contents')

@section('contents')
<div class="ui grid" style="min-height:700px;">
    @include('profile.home.side-menu') 
<div class="ten wide column">
    <div class="column">
        <div class="ui piled segment" style="height300px;margin-top:55px;">
            <div class="ui grid">
                <div class="four wide column">
                    <img src="{{$bk->thumbnail?asset('storage/'.$bk->thumbnail):asset('img/book-placeholder.png')}}" style="max-width:150px;" alt="">
                </div>
                <div class="twelve wide column">
                    <div class="conetnt">
                        
                        <div class="header"><strong>{{$bk->title}}</strong></div>
                        <div class="meta">
                          <span class="date">{{$bk->publisher}}</span>
                        </div>
                        <div class="description">
                        {{$bk->author}}
                        </div>
                        <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam maecenas sed enim ut. Ac turpis egestas integer eget aliquet nibh. Commodo nulla facilisi nullam vehicula ipsum a arcu. Nulla pellentesque dignissim enim sit amet venenatis. Consectetur libero id faucibus nisl. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at augue. Tellus in hac habitasse platea dictumst vestibulum rhoncus. Ut eu sem integer vitae justo. Odio ut sem nulla pharetra diam sit amet nisl suscipit.
                        </div><br>
                        <a href="{{url('/profile/book/download',$bk->id)}}" class="ui positive button">Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h4>Similar books</h4>
    <div class="ui four column grid">
        @foreach($books as $book)
            @include('profile.layouts.book')
        @endforeach
    </div>
</div>
    <div class="three wide column"></div>
</div>
 

@endsection
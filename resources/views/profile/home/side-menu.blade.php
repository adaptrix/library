@php
    use App\Book;
    $books = Book::take(4)->get();
    $i=1;
@endphp
<div class="ui three wide column" style="margin-top:50px;"> 
        <div class="ui bg-red vertical menu" style="margin-left:10px;">
            {{-- <div class="item">
                <div class="ui circular segment">
                    <img class="ui small circular image" src="{{ asset('storage/profile_images/img.jpg') }}">
                </div>
            </div> --}}
            <div class="item">
                <div class="header">{{Auth::user()->name}}</div>
                <div class="menu">
                    <a class="item">View Profile</a>
                </div>
            </div> 
        </div> 
        <div class="ui vertical menu" style="margin-left:10px;">
            {{-- <div class="item">
                <div class="ui circular segment">
                    <img class="ui small circular image" src="{{ asset('storage/profile_images/img.jpg') }}">
                </div>
            </div> --}}
            <div class="item">
                <div class="header">Categories</div>
                @foreach($categories as $category)
            <a href="" class="item">
                {{$category->name}}
            </a> 
            @endforeach
            </div>
           
        </div> 
        <div class="ui vertical menu" style="margin-left:10px;">
            {{-- <div class="item">
                <div class="ui circular segment">
                    <img class="ui small circular image" src="{{ asset('storage/profile_images/img.jpg') }}">
                </div>
            </div> --}}
            <div class="item">
                <div class="header">Top Downloaded books</div>
                @foreach($books as $book)
                <a href="" class="item">
                        {{ $i.". ".str_limit($book->title, $limit = 15, $end = '...') }}
                </a> 
                @php $i++; @endphp
                @endforeach
            </div>
            
        </div> 
</div>
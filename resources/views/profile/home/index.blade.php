@php
    use App\Category;
    use App\Book;
    $books = Book::all();
    $categories = Category::all();
@endphp
@extends('profile.home.contents')

@section('contents')
<div class="ui grid" style="min-height:700px;">
    @include('profile.home.side-menu')
    @include('profile.home.books')
    <div class="three wide column"></div>
</div>
 

@endsection
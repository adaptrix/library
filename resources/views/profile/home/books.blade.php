<div class="ten wide column" style="padding-top:65px;">
    <div class="ui fluid action input" style="margin-bottom:20px;">
        <input type="text" placeholder="Search book..">
        <button class="ui teal right labeled icon button">
            <i class="search icon"></i>
            Search
        </button>
    </div>

    <div class="ui four column grid">
        @foreach($books as $book)
            @include('profile.layouts.book')
        @endforeach
    </div>
</div>

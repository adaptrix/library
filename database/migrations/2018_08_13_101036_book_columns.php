<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            //
            $table->string('author');
            $table->string('publisher');
            $table->integer('category_id');
            $table->string('url');
            $table->string("title");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            //
            $table->dropColumn('author');
            $table->dropColumn('publisher');
            $table->dropColumn('category_id');
            $table->dropColumn('url');
            $table->dropColumn('title');
        });
    }
}

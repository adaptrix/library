<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_superadmin = Role::whereName('superadmin')->first();
        $role_user = Role::whereName('user')->first(); 

        $user = new User();
        $user->name = 'Super Admin';
        $user->email = 'superadmin@library.com';
        $user->password = bcrypt('?.<mNbVcXz|');
        $user->save();
        $user->roles()->attach($role_superadmin);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@library.com';
        $user->password = bcrypt('?.<mNbVcXz|');
        $user->save();
        $user->roles()->attach($role_user);
    }
}
